#!/usr/bin/env python

from argparse import ArgumentParser
import cv2
import apriltag
import copy

import numpy as np

"""params in centimeters"""
TAG_SIZE = 10
CAMERA_TAG_DISTANCE = 80

"""params in pixels"""
IMAGE_WIDTH = 640
IMAGE_HEIGHT = 480
GUESS_LIMIT_DETECTION = 40


def main():

    parser = ArgumentParser(description='test apriltag Python bindings')
    parser.add_argument('device_or_movie', metavar='INPUT', nargs='?', default=0, help='Movie to load or integer ID of camera device')

    apriltag.add_arguments(parser)
    options = parser.parse_args()

    try:
        cap = cv2.VideoCapture(int(options.device_or_movie))
    except ValueError:
        cap = cv2.VideoCapture(options.device_or_movie)

    window = 'Camera'
    cv2.namedWindow(window)


    detector = apriltag.Detector(options)

    toFindAlt = False
    corners1 = [[-1, -1], [-1, -1], [-1, -1], [-1, -1]]
    
    while True:
        success, frame = cap.read()
        if not success:
            break

        gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        detections, dimg = detector.detect(gray, return_image=True)

        num_detections = len(detections)

        for i, detection in enumerate(detections):
            toFindAlt, corners1, corners2 = update_squares_success(corners1, detection)
            lastFrame = copy.deepcopy(frame)
            mtx, dist, rvecs, tvecs = getRvecTvec(detection.corners)
            frame = draw3DCube(frame, detection.corners, mtx, dist, rvecs, tvecs)
            rotations = getRotationsAroundAxes(rvecs)
            frame = printMoveCommandToFrame(detection.center, rotations, tvecs[2], frame, True)


        if num_detections == 0 and toFindAlt:
            center, corners1, frame, lastFrame, toFindAlt = matching_alt_algorithm(lastFrame, corners1, frame, detection.center)
            mtx, dist, rvecs, tvecs = getRvecTvec(corners1)
            frame = draw3DCube(frame, corners1, mtx, dist, rvecs, tvecs)
            rotations = getRotationsAroundAxes(rvecs)
            frame = printMoveCommandToFrame(np.array(center), rotations, tvecs[2], frame, False)

        overlay = frame / 2 + dimg[:, :, None] / 2

        cv2.imshow(window, overlay)
        k = cv2.waitKey(1)

        if k == 27:
            break





def printMoveCommandToFrame(center, rotations, distance, frame, not_guess):

    font = cv2.FONT_HERSHEY_SIMPLEX
    start_text = 300    #in pixels
    text_margin = 35    #in pixels
    margin_center = 20  #in pixels
    margin_distance = 30 #in centimeters
    frame_width_center = IMAGE_WIDTH/2
    frame_height_center = IMAGE_HEIGHT/2

    if (distance > CAMERA_TAG_DISTANCE + margin_distance) & not_guess:
        print "go forward"
        cv2.putText(frame, 'go forward', (10, start_text), font, 1, (255, 255, 255), 2, cv2.LINE_AA)
        start_text += text_margin
    else:
        if (distance < CAMERA_TAG_DISTANCE - margin_distance) & not_guess:
            print "go backward"
            cv2.putText(frame, 'go backward', (10, start_text), font, 1, (255, 255, 255), 2, cv2.LINE_AA)
            start_text += text_margin

    if center[0] > frame_width_center+margin_distance:
        print "-------------go right"
        cv2.putText(frame, 'go right', (10, start_text), font, 1, (255, 255, 255), 2, cv2.LINE_AA)
        start_text += text_margin
    else:
        if center[0] < frame_height_center-margin_distance:
            print "-------------go left"
            cv2.putText(frame, 'go left', (10, start_text), font, 1, (255, 255, 255), 2, cv2.LINE_AA)
            start_text += text_margin
        else:
            start_text = rotate_left_right(rotations[1], start_text, font, frame, text_margin, not_guess)

    if center[1] < frame_height_center-margin_center:
        print "-------------go up"
        cv2.putText(frame, 'go up', (10, start_text), font, 1, (255, 255, 255), 2, cv2.LINE_AA)
        start_text += text_margin
    else:
        if center[1] > frame_height_center+margin_center:
            print "-------------go down"
            cv2.putText(frame, 'go down', (10, start_text), font, 1, (255, 255, 255), 2, cv2.LINE_AA)
            start_text += text_margin
        else:
            rotate_up_down(rotations[0], start_text, font, frame, text_margin, not_guess)
    return frame

def rotate_up_down(rotationAroundXAxes, start_text, font, frame, margin, not_guess):
    margin_rotation = 8  # in degrees
    rotationAroundXAxesP = str(int(abs(rotationAroundXAxes)))
    if (rotationAroundXAxes > margin_rotation) & not_guess:
        print "rotate down " + rotationAroundXAxesP
        cv2.putText(frame, 'rotate down ' + rotationAroundXAxesP, (10, start_text), font, 1, (255, 255, 255), 2, cv2.LINE_AA)
        start_text += margin
    else:
        if (rotationAroundXAxes < -margin_rotation) & not_guess:
            print "rotate up " + rotationAroundXAxesP
            cv2.putText(frame, 'rotate up ' + rotationAroundXAxesP, (10, start_text), font, 1, (255, 255, 255), 2, cv2.LINE_AA)
            start_text += margin
    return start_text

def rotate_left_right(rotationAroundYAxes, start_text, font, frame, margin, not_guess):
    margin_rotation = 8    #in degrees
    rotationAroundYAxesP = str(int(abs(rotationAroundYAxes)))
    if (rotationAroundYAxes < -margin_rotation) & not_guess:
        print "rotate right " + rotationAroundYAxesP
        cv2.putText(frame, 'rotate right ' + rotationAroundYAxesP, (10, start_text), font, 1, (255, 255, 255), 2, cv2.LINE_AA)
        start_text += margin
    else:
        if (rotationAroundYAxes > margin_rotation) & not_guess:
            print "rotate left " + rotationAroundYAxesP
            cv2.putText(frame, 'rotate left ' + rotationAroundYAxesP, (10, start_text), font, 1, (255, 255, 255), 2, cv2.LINE_AA)
            start_text += margin
    return start_text

def getRotationsAroundAxes(rvec):
    Rw2c, jacob = cv2.Rodrigues(np.array(rvec))
    Rc2w = np.transpose(Rw2c)
    ones = np.array([[-1, 0 , 0], [0, 1 , 0], [0, 0, -1]])
    R1 = np.dot(ones, Rc2w)
    retval, mtxR, mtxQ, Qx, Qy, Qz =cv2.RQDecomp3x3(R1)
    return retval

def draw(img, corners, imgpts):
    corners = np.array(corners, int)
    corner = tuple(corners[0].ravel())
    img = cv2.line(img, corner, tuple(imgpts[0].ravel()), (255, 0, 0), 5)
    img = cv2.line(img, corner, tuple(imgpts[1].ravel()), (0, 255, 0), 5)
    img = cv2.line(img, corner, tuple(imgpts[2].ravel()), (0, 0, 255), 5)

    imgpts = np.int32(imgpts).reshape(-1, 2)
    img = cv2.drawContours(img, [imgpts[:4]], -1, (0, 255, 0), -3)

    for i,j in zip(range(4), range(4, 8)):
        img = cv2.line(img, tuple(imgpts[i]), tuple(imgpts[j]), (255), 3)

    img = cv2.drawContours(img, [imgpts[4:]], -1, (0, 0, 255), 3)
    return img

def draw3DCube(img, corners, mtx, dist, rvecs, tvecs):
    axis = np.float32([[0, TAG_SIZE, 0], [TAG_SIZE, TAG_SIZE, 0], [TAG_SIZE, 0, 0], [0, 0, 0],
                       [0, TAG_SIZE, TAG_SIZE], [TAG_SIZE, TAG_SIZE, TAG_SIZE], [TAG_SIZE, 0, TAG_SIZE], [0, 0, TAG_SIZE]])

    #get all the eight corners of the cube
    imgpts, jac = cv2.projectPoints(axis, np.array(rvecs), np.array(tvecs), mtx, dist)
    img = draw(img, corners, imgpts)
    return img

def getCalibrationDetails(corners):
    imgpoints = []
    objpoints = []

    objpoints.append(np.array([[0, TAG_SIZE, 0], [TAG_SIZE, TAG_SIZE, 0], [TAG_SIZE, 0, 0], [0, 0, 0]], "float32"))
    imagePoints = []

    for l in corners:
        imagePoints.append([l])

    imgpoints.append(np.array(imagePoints, "float32"))
    imageSize = (IMAGE_WIDTH, IMAGE_WIDTH)

    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, imageSize, None, None)
    return ret, mtx, dist, rvecs, tvecs

def getRvecTvec(corners):
    #the World Coordinates
    objpoints = np.array([[0, TAG_SIZE, 0], [TAG_SIZE, TAG_SIZE, 0], [TAG_SIZE, 0, 0], [0, 0, 0]], "float32")
    imagePoints = []
    for l in corners:
        imagePoints.append([l])

    #this params evaluated using calibrate camera
    mtx = np.array([[616.8916564, 0, 319.49999998], [0, 607.55867679, 239.50000002], [0, 0, 1]], "float32")
    dist = np.array([[-2.67410942e-09, -6.03583369e-08, 6.60027792e-10, -1.14305831e-09, -1.28975745e-06]], "float32")

    retval, rvec, tvec = cv2.solvePnP(objpoints, np.array(imagePoints, "float32"), mtx, dist)
    return mtx, dist, rvec, tvec

def matching_alt_algorithm(lastFrame, corners, frame, center):
    orb = cv2.ORB_create()
    kp1, des1 = orb.detectAndCompute(lastFrame, None)
    kp2, des2 = orb.detectAndCompute(frame, None)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck = True)
    matches = bf.match(des1, des2)

    matchesFixed = []
    for mat in matches:
        if pointInPolygon(kp1[mat.queryIdx].pt, corners):
            matchesFixed.append(mat)

    matchesFixed = sorted(matchesFixed, key = lambda x:x.distance)

    num_of_matches = 100
    list_kp1 = []
    list_kp2 = []

    for mat in matchesFixed[:num_of_matches]:

        img1_idx = mat.queryIdx
        img2_idx = mat.trainIdx

        (x1, y1) = kp1[img1_idx].pt
        (x2, y2) = kp2[img2_idx].pt

        point1 = [x1, y1]
        if pointInPolygon(point1, corners):
            list_kp1.append((x1, y1))
            list_kp2.append((x2, y2))

    if len(list_kp1) == 0 or matchesFixed[0].distance > GUESS_LIMIT_DETECTION:
        return center, corners, frame, lastFrame, False

    dx = list_kp2[0][0] - list_kp1[0][0]
    dy = list_kp2[0][1] - list_kp1[0][1]

    result = map(lambda x: [x[0] + dx, x[1] + dy], corners)

    M = np.float32([[1, 0, dx], [0, 1, dy]])
    lastFrame = cv2.warpAffine(lastFrame, M, (IMAGE_WIDTH, IMAGE_HEIGHT))

    x = int(np.average(map(lambda x: x[0], result)))
    y = int(np.average(map(lambda x: x[1], result)))
    center = (x, y)

    return center, result, frame, lastFrame, True


def pointInPolygon(point, polygon):
    x = point[0]
    y = point[1]
    x1 = polygon[0][0]
    x2 = polygon[2][0]
    y1 = polygon[0][1]
    y2 = polygon[2][1]
    if x > x1 and x < x2 and y > y1 and y < y2:
        return True
    return False

def drow_square(frame, corners, color, thickness):
    pts = np.array(corners, np.int32)
    cv2.polylines(frame,[pts],True,color, thickness)
    return frame

def update_squares_success(corners1, detection):
    corners2 = corners1
    corners1 = detection.corners

    return True, corners1, corners2





if __name__ == '__main__':
    main()
