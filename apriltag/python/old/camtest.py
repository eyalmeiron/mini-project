#!/usr/bin/env python

'''Demonstrate Python wrapper of C apriltag library by running on camera frames.'''

from argparse import ArgumentParser
import cv2
import apriltag
from PIL import Image
import time
import numpy as np
from matplotlib import pyplot as plt

def save_images(detections, frame):
    if detections != []:
        # time.sleep(2)
        result = Image.fromarray(frame)
        result.save('success/work' + time.strftime("%Y-%m-%d-%H:%M:%S") + '.jpg')
    else:
        result = Image.fromarray(frame)
        result.save('fail/work' + time.strftime("%Y-%m-%d-%H:%M:%S") + '.jpg')

def main():

    '''Main function.'''

    parser = ArgumentParser(
        description='test apriltag Python bindings')

    parser.add_argument('device_or_movie', metavar='INPUT', nargs='?', default=0,
                        help='Movie to load or integer ID of camera device')

    apriltag.add_arguments(parser)

    options = parser.parse_args()

    try:
        cap = cv2.VideoCapture(int(options.device_or_movie))
    except ValueError:
        cap = cv2.VideoCapture(options.device_or_movie)

    window = 'Camera'
    cv2.namedWindow(window)

    # overlayW = 'Overlay'
    # cv2.namedWindow(overlayW)

    detector = apriltag.Detector(options)

    im_out=1
    counter = 0
    image1 = -1
    image2 = -1
    corners1 = [[-1, -1], [-1, -1], [-1, -1], [-1, -1]]
    corners2 = [[-1, -1], [-1, -1], [-1, -1], [-1, -1]]
    corners3 = [[-1, -1], [-1, -1], [-1, -1], [-1, -1]]

    while True:
        counter = counter + 1

        success, frame = cap.read()
        if not success:
            break

        gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        detections, dimg = detector.detect(gray, return_image=True)

        num_detections = len(detections)

        delay = 20
        if counter % delay == 0:
            print 'Detected {} tags.'.format(num_detections)

            for i, detection in enumerate(detections):
                # im_out = makePerspective(detection, frame, im_out)
                printing(num_detections, i, detection, image1/delay, image2/delay, corners1, corners2)
                image1, image2, corners1, corners2, corners3, frame, counter, detection = update_squares_success(image1, image2, corners1, corners2, corners3, frame, counter, detection)
                lastFrame = frame

            if num_detections == 0 and image1 >= 0:
                corners2 = corners1
                corners1, frame = one_matching_alg(lastFrame, frame, corners1, frame)
                # corners1, corners2, frame, counter = update_squares_fail(corners1, corners2, frame, counter)
                #frame = show_guess_detection(corners1, frame)
		

            overlay = frame / 2 + dimg[:, :, None] / 2

            #save_images(detections, frame)

            # cv2.imshow(overlayW, im_out)
            cv2.imshow(window, overlay)


        k = cv2.waitKey(0)

        if k == 27:
            break

def printing(num_detections, i, detection, image1, image2, corners1, corners2):
    # print 'first image: {}'.format(image1)
    # print corners1
    # print 'second image: {}'.format(image2)
    # print corners2
    print ''

    # print_move_command(detection)

    # print_frame_detections(num_detections, i, detection)


def angle_between(p1, p2):
    ang1 = np.arctan2(*p1[::-1])
    ang2 = np.arctan2(*p2[::-1])
    return np.rad2deg((ang1 - ang2) % (2 * np.pi))

def get_list_of_angle(list_kp1, list_kp2):
	list_ans = [] 
	return list_ans


	
def one_matching_alg(img1, img2, corners, frame):
    orb = cv2.ORB_create()
    kp1, des1 = orb.detectAndCompute(img1,None)
    kp2, des2 = orb.detectAndCompute(img2,None)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

    matches = bf.match(des1,des2)
    matches = sorted(matches, key = lambda x:x.distance)

    num_of_matches = 1
    matchesFixed = []
    for mat in matches:
        if pointInPolygon(kp1[mat.queryIdx].pt, corners):
            matchesFixed.append(mat)

    img3 = cv2.drawMatches(img1,kp1,img2,kp2,matchesFixed[:num_of_matches], None, flags=2)

    list_kp1 = []
    list_kp2 = []
    x_diff = 0
    y_diff = 0

    for mat in matchesFixed[:num_of_matches]:

        img1_idx = mat.queryIdx
        img2_idx = mat.trainIdx

        (x1,y1) = kp1[img1_idx].pt
        (x2,y2) = kp2[img2_idx].pt
        x_diff = x2 - x1
        y_diff = y2 - y1

        corners[0][0] = corners[0][0]+ x_diff
        corners[0][1] = corners[0][1]+ y_diff
        corners[1][0] = corners[1][0]+ x_diff
        corners[1][1] = corners[1][1]+ y_diff
        corners[2][0] = corners[2][0]+ x_diff
        corners[2][1] = corners[2][1]+ y_diff
        corners[3][0] = corners[3][0]+ x_diff
        corners[3][1] = corners[3][1]+ y_diff

        src = np.array(corners, dtype=np.float32)
        frame = drow_square(frame, src, (0, 0, 255), 3) # red
        #plt.imshow(img3),plt.show()

    return corners, frame



def matching_alt_algorithm(img1, img2, corners, frame):
    orb = cv2.ORB_create()
    kp1, des1 = orb.detectAndCompute(img1,None)
    kp2, des2 = orb.detectAndCompute(img2,None)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

    matches = bf.match(des1,des2)
    matches = sorted(matches, key = lambda x:x.distance)

    num_of_matches = 50
    matchesFixed = []
    for mat in matches:
        if pointInPolygon(kp1[mat.queryIdx].pt, corners):
            matchesFixed.append(mat)

    img3 = cv2.drawMatches(img1,kp1,img2,kp2,matchesFixed[:num_of_matches], None, flags=2)

    list_kp1 = []
    list_kp2 = []

    for mat in matchesFixed[:num_of_matches]:

        img1_idx = mat.queryIdx
        img2_idx = mat.trainIdx

        (x1,y1) = kp1[img1_idx].pt
        (x2,y2) = kp2[img2_idx].pt

        point1 = [x1, y1]
        if pointInPolygon(point1 ,corners):
            list_kp1.append((x1, y1))
            list_kp2.append((x2, y2))

    # print "min: "
    # print min(list_kp1)
    # print "max: "
    # print max(list_kp1)
    # print "\n"

    if len(list_kp1) >= 4:
        points1, points2 = choosePoints(list_kp1, list_kp2)

        pts1 = np.float32(points1)
        pts2 = np.float32(points2)
        M = cv2.getPerspectiveTransform(pts1,pts2)

        src = np.array(corners, dtype=np.float32)
        result = cv2.perspectiveTransform(src[None, :, :], M)

        frame = drow_square(frame, result, (0, 0, 255), 3) # red

        plt.imshow(img3),plt.show()

        return result[0], frame
    return corners, frame

def choosePoints(list_kp1, list_kp2):
    points1 = [[-1, -1], [-1, -1], [-1, -1], [-1, -1]]
    points2 = [[-1, -1], [-1, -1], [-1, -1], [-1, -1]]

    points1[0] = sorted(list_kp1, key = lambda x:x[0])[0]
    points1[1] = sorted(list_kp1, key = lambda x:x[0], reverse = True)[0]
    points1[2] = sorted(list_kp1, key = lambda x:x[1])[0]
    points1[3] = sorted(list_kp1, key = lambda x:x[1], reverse = True)[0]

    points2[0] = sorted(list_kp2, key = lambda x:x[0])[0]
    points2[1] = sorted(list_kp2, key = lambda x:x[0], reverse = True)[0]
    points2[2] = sorted(list_kp2, key = lambda x:x[1])[0]
    points2[3] = sorted(list_kp2, key = lambda x:x[1], reverse = True)[0]

    points1 = list_kp1[:4]
    points2 = list_kp2[:4]

    print points1
    print points2

    return points1, points2

def pointInPolygon(point, polygon):
    x = point[0]
    y = point[1]
    x1 = polygon[0][0]
    x2 = polygon[2][0]
    y1 = polygon[0][1]
    y2 = polygon[2][1]
    if x > x1 and x < x2 and y > y1 and y < y2:
        return True
    return False

def show_guess_detection(corners, frame):
    x1 = min(corners[0][0], corners[1][0], corners[2][0], corners[3][0])
    x2 = max(corners[0][0], corners[1][0], corners[2][0], corners[3][0])
    y1 = min(corners[0][1], corners[1][1], corners[2][1], corners[3][1])
    y2 = max(corners[0][1], corners[1][1], corners[2][1], corners[3][1])

    d_x = (x2-x1)/5
    d_y = (y2-y1)/5

    x1 = x1 - d_x
    x2 = x2 + d_x
    y1 = y1 - d_y
    y2 = y2 + d_y

    new_corners = [[x1,y2],[x2,y2],[x2,y1],[x1,y1]]
    frame = drow_square(frame, new_corners, (255, 255, 0), 1)

    return frame

def drow_square(frame, corners, color, thickness):
    pts = np.array(corners, np.int32)
    cv2.polylines(frame,[pts],True,color, thickness)
    return frame

def print_frame_detections(num_detections, i, detection):
    print 'Detection {} of {}:'.format(i+1, num_detections)
    print
    print detection.tostring(indent=2)
    print

def update_squares_fail(corners1, corners2, frame, counter):
    pts1 = np.float32(corners1)
    pts2 = np.float32(corners2)
    M = cv2.getPerspectiveTransform(pts2,pts1)

    src = np.array(corners1, dtype=np.float32)
    result = cv2.perspectiveTransform(src[None, :, :], M)
    corners2 = corners1
    corners1 = result[0]

    pts1 = np.float32(corners1)
    pts2 = np.float32(corners2)
    M = cv2.getPerspectiveTransform(pts2,pts1)

    src = np.array(corners1, dtype=np.float32)
    result = cv2.perspectiveTransform(src[None, :, :], M)

    # frame = drow_square(frame, result, (0, 0, 255), 2) # red
    frame = drow_square(frame, corners1, (0, 255, 0), 2) # green
    # frame = drow_square(frame, corners2, (255, 0, 0), 2) # blue

    return corners1, corners2, frame, counter

def update_squares_success(image1, image2, corners1, corners2, corners3, frame, counter, detection):
    image2 = image1
    image1 = counter
    corners3 = corners2
    corners2 = corners1
    corners1 = detection.corners

    pts1 = np.float32(corners1)
    pts2 = np.float32(corners2)
    M = cv2.getPerspectiveTransform(pts2,pts1)

    src = np.array(corners1, dtype=np.float32)
    result = cv2.perspectiveTransform(src[None, :, :], M)

    frame = drow_square(frame, result, (0, 0, 255), 3) # red
    frame = drow_square(frame, corners1, (0, 255, 0), 2) # green
    frame = drow_square(frame, corners2, (255, 0, 0), 2) # blue
    # frame = drow_square(frame, corners3, (255, 0, 255), 2) # purple

    return image1, image2, corners1, corners2, corners3, frame, counter, detection

def print_move_command(detection):
    x_right_up = detection.corners[1][0]
    x_left_up = detection.corners[0][0]
    y_right_up = detection.corners[1][1]
    y_left_up = detection.corners[0][1]
    x_left_down = detection.corners[3][0]
    x_right_down = detection.corners[2][0]
    y_left_down = detection.corners[3][1]
    y_right_down = detection.corners[2][1]
    dist_up = np.math.hypot(x_right_up - x_left_up, y_right_up - y_left_up)
    dist_down = np.math.hypot(x_right_down - x_left_down, y_right_down - y_left_down)
    dist_right = np.math.hypot(x_right_up - x_right_down, y_right_up - y_left_down)
    dist_left = np.math.hypot(x_left_up - x_left_down, y_left_up - y_left_down)

    scale = getScale(detection)

    #print scale
    #if (scale[0]>0.8):      #not working well
    #    print "go forword"
    #else:
        #if (scale[0]<0.4):
        #    print "go backword"
        #else:
    if detection.center[0] > 335:
        print "-------------go right"
    if detection.center[0] < 305:
        print "-------------go left"
    else:
        if detection.center[1] < 225:
            print "-------------go up"
        if detection.center[1] > 255:
            print "-------------go down"
        else:
            if (dist_up - dist_down) > 2:
                print "rotate up"
            if (dist_up - dist_down) < -2:
                print "rotate down"
            else:
                if (dist_right - dist_left) > 2:
                    print "rotate left"
                if (dist_right - dist_left) < -2:
                    print "rotate right"

def getScale(detection):
    normalised_homography = getHomography(detection)
    a = normalised_homography[0, 0]
    b = normalised_homography[0, 1]
    c = normalised_homography[0, 2]
    d = normalised_homography[1, 0]
    e = normalised_homography[1, 1]
    f = normalised_homography[1, 2]
    p = np.math.sqrt(a * a + b * b)
    r = (a * e - b * d) / (p)
    q = (a * d + b * e) / (a * e - b * d)
    translation = (c, f)
    scale = (p, r)
    shear = q
    theta = np.math.atan2(b, a)
    inAngle = theta * 180 / np.math.pi

    return scale

def makePerspective(detection, frame, im_out):
    # Four corners of the book in source image
    h = getHomography(detection)
    print h
    # Warp source image to destination based on homography
    im_out = cv2.warpPerspective(frame, h, (frame.shape[1], frame.shape[0]))
    return im_out

def getHomography(detection):
    pts_src = np.array([detection.corners])
    # Four corners of the book in destination image.
    pts_dst = np.array([[300, 220], [333, 220], [333, 250], [300, 250]])
    # Calculate Homography
    h, status = cv2.findHomography(pts_src, pts_dst)
    return h



if __name__ == '__main__':
    main()


# def matching_alt_algorithm(img1, img2, corners):
#     # Initiate SIFT detector
#     orb = cv2.ORB_create()
#
#     # find the keypoints and descriptors with SIFT
#     kp1, des1 = orb.detectAndCompute(img1,None)
#     kp2, des2 = orb.detectAndCompute(img2,None)
#
#     # create BFMatcher object
#     bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
#
#     # Match descriptors.
#     matches = bf.match(des1,des2)
#
#     # Sort them in the order of their distance.
#     matches = sorted(matches, key = lambda x:x.distance)
#
#     # Draw first 10 matches.
#     img3 = cv2.drawMatches(img1,kp1,img2,kp2,matches[:100], None, flags=2)
#
#     plt.imshow(img3),plt.show()
#
#     return new_corners
