#!/usr/bin/env python

'''Demonstrate Python wrapper of C apriltag library by running on camera frames.'''

from argparse import ArgumentParser
import cv2
import apriltag
from PIL import Image
import time
import numpy as np

# for some reason pylint complains about members being undefined :(
# pylint: disable=E1101


def save_images(detections, frame):
    if detections != []:
        # time.sleep(2)
        result = Image.fromarray(frame)
        result.save('success/work' + time.strftime("%Y-%m-%d-%H:%M:%S") + '.jpg')
    else:
        result = Image.fromarray(frame)
        result.save('fail/work' + time.strftime("%Y-%m-%d-%H:%M:%S") + '.jpg')


def main():

    '''Main function.'''

    parser = ArgumentParser(
        description='test apriltag Python bindings')

    parser.add_argument('device_or_movie', metavar='INPUT', nargs='?', default=0,
                        help='Movie to load or integer ID of camera device')

    apriltag.add_arguments(parser)

    options = parser.parse_args()

    try:
        cap = cv2.VideoCapture(int(options.device_or_movie))
    except ValueError:
        cap = cv2.VideoCapture(options.device_or_movie)

    window = 'Camera'
    cv2.namedWindow(window)

    overlayW = 'Overlay'
    cv2.namedWindow(overlayW)

    detector = apriltag.Detector(options)

    im_out=1
    counter = 0
    image1 = -1
    image2 = -1
    corners1 = [[-1, -1], [-1, -1], [-1, -1], [-1, -1]]
    corners2 = [[-1, -1], [-1, -1], [-1, -1], [-1, -1]]
    corners3 = [[-1, -1], [-1, -1], [-1, -1], [-1, -1]]

    while True:
        # time.sleep(0.2)
        counter = counter + 1

        success, frame = cap.read()
        if not success:
            break


        gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        detections, dimg = detector.detect(gray, return_image=True)

        num_detections = len(detections)

        delay = 5
        if counter % delay == 0:
            print 'Detected {} tags.'.format(num_detections)

            for i, detection in enumerate(detections):
                # print 'Detection {} of {}:'.format(i+1, num_detections)
                # print
                # print detection.tostring(indent=2)
                # print
                # im_out = makePerspective(detection, frame, im_out)
                #inAngle = getRotation(detection)
                # print_move_command(detection)
                image1, image2, corners1, corners2, frame, counter, detection = update_squares_success(image1, image2, corners1, corners2, frame, counter, detection)

            if num_detections == 0 and image1 >= 0:
                corners1, corners2, frame, counter = update_squares_fail(corners1, corners2, frame, counter)

            print 'index {}'.format(counter/delay)
            print 'first image: {}'.format(image1/delay)
            print corners1
            print 'second image: {}'.format(image2/delay)
            print corners2
            print '\n'

            overlay = frame / 2 + dimg[:, :, None] / 2

            #save_images(detections, frame)

            # cv2.imshow(overlayW, im_out)
            cv2.imshow(window, overlay)


        k = cv2.waitKey(1)

        if k == 27:
            break



def update_squares_fail(corners1, corners2, frame, counter):
    pts1 = np.float32(corners1)
    pts2 = np.float32(corners2)
    M = cv2.getPerspectiveTransform(pts2,pts1)

    src = np.array(corners1, dtype=np.float32)
    result = cv2.perspectiveTransform(src[None, :, :], M)
    corners2 = corners1
    corners1 = result[0]

    pts1 = np.float32(corners1)
    pts2 = np.float32(corners2)
    M = cv2.getPerspectiveTransform(pts2,pts1)

    src = np.array(corners1, dtype=np.float32)
    # result = cv2.perspectiveTransform(src[None, :, :], M)

    pts = np.array(result, np.int32)
    cv2.polylines(frame,[pts],True,(0,0,255), 2) # red

    pts = np.array(corners1, np.int32)
    cv2.polylines(frame,[pts],True,(0,255,0), 2) # green (lead)

    pts = np.array(corners2, np.int32)
    cv2.polylines(frame,[pts],True,(255,0,0), 2) # blue
    return corners1, corners2, frame, counter

def update_squares_success(image1, image2, corners1, corners2, frame, counter, detection):
    image2 = image1
    corners2 = corners1
    image1 = counter
    corners1 = detection.corners

    pts1 = np.float32(corners1)
    pts2 = np.float32(corners2)
    M = cv2.getPerspectiveTransform(pts2,pts1)

    src = np.array(corners1, dtype=np.float32)
    result = cv2.perspectiveTransform(src[None, :, :], M)

    pts = np.array(result, np.int32)
    cv2.polylines(frame,[pts],True,(0,0,255), 2) # red

    pts = np.array(corners1, np.int32)
    cv2.polylines(frame,[pts],True,(0,255,0), 2) # green (lead)

    pts = np.array(corners2, np.int32)
    cv2.polylines(frame,[pts],True,(255,0,0), 2) # blue
    return image1, image2, corners1, corners2, frame, counter, detection



def print_move_command(detection):
    x_right_up = detection.corners[1][0]
    x_left_up = detection.corners[0][0]
    y_right_up = detection.corners[1][1]
    y_left_up = detection.corners[0][1]
    x_left_down = detection.corners[3][0]
    x_right_down = detection.corners[2][0]
    y_left_down = detection.corners[3][1]
    y_right_down = detection.corners[2][1]
    dist_up = np.math.hypot(x_right_up - x_left_up, y_right_up - y_left_up)
    dist_down = np.math.hypot(x_right_down - x_left_down, y_right_down - y_left_down)
    dist_right = np.math.hypot(x_right_up - x_right_down, y_right_up - y_left_down)
    dist_left = np.math.hypot(x_left_up - x_left_down, y_left_up - y_left_down)

    scale = getScale(detection)

    #print scale
    #if (scale[0]>0.8):      #not working well
    #    print "go forword"
    #else:
        #if (scale[0]<0.4):
        #    print "go backword"
        #else:
    if detection.center[0] > 335:
        print "-------------go right"
    if detection.center[0] < 305:
        print "-------------go left"
    else:
        if detection.center[1] < 225:
            print "-------------go up"
        if detection.center[1] > 255:
            print "-------------go down"
        else:
            if (dist_up - dist_down) > 2:
                print "rotate up"
            if (dist_up - dist_down) < -2:
                print "rotate down"
            else:
                if (dist_right - dist_left) > 2:
                    print "rotate left"
                if (dist_right - dist_left) < -2:
                    print "rotate right"


def getScale(detection):
    normalised_homography = getHomography(detection)
    a = normalised_homography[0, 0]
    b = normalised_homography[0, 1]
    c = normalised_homography[0, 2]
    d = normalised_homography[1, 0]
    e = normalised_homography[1, 1]
    f = normalised_homography[1, 2]
    p = np.math.sqrt(a * a + b * b)
    r = (a * e - b * d) / (p)
    q = (a * d + b * e) / (a * e - b * d)
    translation = (c, f)
    scale = (p, r)
    shear = q
    theta = np.math.atan2(b, a)
    inAngle = theta * 180 / np.math.pi

    return scale


def makePerspective(detection, frame, im_out):
    # Four corners of the book in source image
    h = getHomography(detection)
    print h
    # Warp source image to destination based on homography
    im_out = cv2.warpPerspective(frame, h, (frame.shape[1], frame.shape[0]))
    return im_out


def getHomography(detection):
    pts_src = np.array([detection.corners])
    # Four corners of the book in destination image.
    pts_dst = np.array([[300, 220], [333, 220], [333, 250], [300, 250]])
    # Calculate Homography
    h, status = cv2.findHomography(pts_src, pts_dst)
    return h



if __name__ == '__main__':
    main()
